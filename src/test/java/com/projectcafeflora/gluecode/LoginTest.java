package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.WebDriverWait;

import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginTest {
	
	public LoginPage loginPage;
	public HomePage homePage;
	
	//**************** FIRST SCENARIO
	@Given("a user is at the login page of Cafe Flora first time")
	public void a_user_is_at_the_login_page_of_cafe_flora_first_time() {
		this.loginPage = new LoginPage(DriverUtility.driver);
		assertEquals(loginPage.getGreeting(), "Hello Customer");
	}

	@When("a user inputs their email {string}")
	public void a_user_inputs_their_email(String string) {
		this.loginPage.setEmail(string);
	}
	
	@When("a user inputs their password {string}")
	public void a_user_inputs_their_password(String string) {
		this.loginPage.setPassword(string);
	}
	
	@When("then the user submits the information")
	public void then_the_user_submits_the_information() throws InterruptedException {
		this.loginPage.login();
//		TimeUnit.SECONDS.sleep(3);
	}
	
	@Then("the user is redirected to home page")
	public void the_user_is_redirected_to_home_page() throws InterruptedException {
		this.homePage = new HomePage(DriverUtility.driver);
		TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/home");
	}
	
	//**************** SECOND SCENARIO
	@Given("a user is at the login page of Cafe Flora second time")
	public void a_user_is_at_the_login_page_of_cafe_flora_second_time() {
		this.loginPage = new LoginPage(DriverUtility.driver);
		assertEquals(loginPage.getGreeting(), "Hello Customer");
	}

	@When("a user clicks the Create a New Account link")
	public void a_user_clicks_the_create_a_new_account_link() {
		this.loginPage.register();
	}
	
	@Then("the user is redirected to the registration page")
	public void the_user_is_redirected_to_the_registration_page() {
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/register");
	}
	
	//**************** THIRD SCENARIO
	@Given("a user is at the login page of Cafe Flora third time")
	public void a_user_is_at_the_login_page_of_cafe_flora_third_time() {
		this.loginPage = new LoginPage(DriverUtility.driver);
		assertEquals(loginPage.getGreeting(), "Hello Customer");
	}

	@When("a user clicks the Forget Password link")
	public void a_user_clicks_the_forget_password_link() {
	    this.loginPage.recoverPassword();
	}
	
	@Then("the user is redirected to the recover password page")
	public void the_user_is_redirected_to_the_recover_password_page() {
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/forget");
	}
	
}
