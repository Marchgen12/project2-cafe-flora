package com.projectcafeflora.gluecode;

import static org.junit.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.projectcafeflora.eval.page.CartPage;
import com.projectcafeflora.eval.page.CheckOutPage;
import com.projectcafeflora.eval.page.HomePage;
import com.projectcafeflora.eval.page.LoginPage;
import com.projectcafeflora.eval.page.MenuPage;
import com.projectcafeflora.eval.page.PaymentPage;
import com.projectcafeflora.eval.page.VerificationPage;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class VerificationTest {
	
	public LoginPage loginPage;
	public HomePage homePage;
	public MenuPage menuPage;
	public CartPage cartPage;
	public CheckOutPage checkoutPage;
	public PaymentPage paymentPage;
	public VerificationPage verifyPage;
	public WebDriver driver = new ChromeDriver();
	
	@When("then a user sees the verification page")
	public void then_a_user_sees_the_verification_page() {
		this.verifyPage = new VerificationPage(DriverUtility.driver);
		assertEquals(this.verifyPage.getCustomer(), "Daniel Butcher");
		assertEquals(this.verifyPage.getDistribution(), "Delivery");
		assertEquals(this.verifyPage.getMessage1(), "Your order will be delivered in roughly 50 minutes");
		assertEquals(this.verifyPage.getMessage2(), "to 123 Main St New City, NY 01012");
	}

	@When("then a user clicks the close verification page button")
	public void then_a_user_clicks_the_close_verification_page_button() {
		this.verifyPage.clickSubmit();
	}
	
	@Then("a user is redirected from the verification page to the home page")
	public void a_user_is_redirected_from_the_verification_page_to_the_home_page() throws InterruptedException {
	    this.homePage = new HomePage(DriverUtility.driver);
	    TimeUnit.SECONDS.sleep(3);
		assertEquals(DriverUtility.driver.getCurrentUrl(), "http://localhost:4200/home");
	}
}
