package com.projectcafeflora.eval.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.projectcafeflora.controller.OrderStatusController;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderStatusService;

@SpringBootTest
public class OrderStatusControllerUnitTest {

	@Mock
	OrderStatusService osServ;
	
	@InjectMocks
	OrderStatusController osCon;
	
	private OrderStatus orderStatus;
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		mock = MockMvcBuilders.standaloneSetup(osCon).build();
		orderStatus = new OrderStatus(1, "Building");
		when(osServ.getById(1)).thenReturn(orderStatus);
		when(osServ.getById(0)).thenReturn(null);
	}
	
	@Test
	public void getOrderStatusSuccess() throws Exception {
		mock.perform(get("/order-statuses/{orderstatusid}", orderStatus.getStatusId()))
			.andExpect(status().isOk()).andExpect(jsonPath("$.statusName", is(orderStatus.getStatusName())));
	}
	
	@Test
	public void getOrderStatusFailure() throws Exception {
		mock.perform(get("/order-statuses/{orderstatusid}", 0))
			.andExpect(status().isNotFound());
	}
	
}
