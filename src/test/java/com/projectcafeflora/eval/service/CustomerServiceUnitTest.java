package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.CustomerDao;
import com.projectcafeflora.dao.MockBankDao;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.service.CustomerService;
import com.projectcafeflora.service.MockBankService;

@SpringBootTest
public class CustomerServiceUnitTest {

	private String encrypted = "S15nC0XLiN9N9fJJR/Im05HUlRZcwbfw";
	private String encryptedCC1 = "MdQyoeUwysSodpzU7kgEMyDqUV5cFAIw159iJwafVZQ=";
	
	@Mock
	private CustomerDao crDao;
	
	@InjectMocks
	private CustomerService crServ;
	
	@InjectMocks
	private CustomerService crServ2 = mock(CustomerService.class);
	
	private Customer customer;
	
	private List<Customer> customers = new ArrayList<Customer>();

	@BeforeEach
	private void setUp() throws Exception{
		crServ = new CustomerService(crDao);
		customer = new Customer(1, "email@email.com", encrypted, "Daniel", "Butcher", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
		customers.add(customer);
		when(crDao.findByCustomerId(1)).thenReturn(customer);
		when(crDao.findByPhone("123 456 7890")).thenReturn(customer);
		when(crDao.findByEmail("email@email.com")).thenReturn(customer);
		when(crDao.findByEmailAndPassword("email@email.com", encrypted)).thenReturn(customer);
		when(crDao.findByEmailAndPassword("email@email.com", "password")).thenReturn(null);
	}
	
	@Test
	public void testFindById() {
		assertEquals(crServ.getById(1), customer);
	}
	
	@Test
	public void testRetrieveEmailByPhoneSuc() {
		assertEquals(crServ.retreiveEmailByPhone("123 456 7890"), "email@email.com");
	}
	
	@Test
	public void testInsertCustomer() {
		crServ2.insertCustomer("email@email.com", encrypted, "Daniel", "Butcher", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
		verify(crServ2, times(1)).insertCustomer("email@email.com", encrypted, "Daniel", "Butcher", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
	}
	
	
	//I am only running a test if it run not if it works because there is a DAO method inside the Service
	//method that I dont know how to access.
	//the is this line `customer = crDao.findByEmail(tempEmail);` but I didnt call this dao method in this test
	@Test
	public void testViewProfile() {
		crServ2.viewProfile("email@email.com", encrypted);
		verify(crServ2, times(1)).viewProfile("email@email.com", encrypted);
	}
	
	
	@Test
	public void testChangePassword() {
		crServ2.changePassword("email@email.com", encrypted);
//		verify(crServ2, times(1)).changePassword("email@email.com", encrypted);
		verify(crServ2).changePassword("email@email.com", encrypted);
//		crServ3.changePassword("email@email.com", "newpassword");
//		assertEquals(customer.getPassword(),"newpassword");
	}
	
	@Test
	public void testUpdateAddress() {
		crServ2.updateAddress("email@email.com", encrypted, "address2", "city2", "state2", "12345");
		verify(crServ2).updateAddress("email@email.com", encrypted, "address2", "city2", "state2", "12345");
	}
	
	@Test
	public void testUpdateCreditCard() {
		crServ2.updateCreditCard("email@email.com", encrypted, "1234568791234567", "creditCardName", 123, 1, 2025);
		verify(crServ2).updateCreditCard("email@email.com", encrypted, "1234568791234567", "creditCardName", 123, 1, 2025);
	}
	
	@Test
	public void testDeleteCustomer() {
		crServ2.deleteCustomer("email@email.com", encrypted);
		verify(crServ2).deleteCustomer("email@email.com", encrypted);
	}
	
	@Test
	public void testCheckCreditCardAccepted() {
		crServ2.checkCreditCard(encryptedCC1, "Daniel Butcher", 999, 1, 2025, 0);
		verify(crServ2).checkCreditCard(encryptedCC1, "Daniel Butcher", 999, 1, 2025, 0);
	}
	
	@Test
	public void testRetrievePassword() {
		assertEquals(crServ.retrievePassword("email@email.com", "Daniel", "Butcher"), "Email sent");
	}
}
