package com.projectcafeflora.eval.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.service.MenuItemService;

@SpringBootTest
public class MenuItemServiceUnitTest {
	
	@Mock
	private MenuItemDao miDao;
	
	@InjectMocks
	private MenuItemService miServ1;
	
	@InjectMocks
	private MenuItemService miServ2 = mock(MenuItemService.class);
	
	private MenuItem menuItem;
	private MenuItem menuItem2;
	private List<MenuItem> miList = new ArrayList<MenuItem>();
	
	@BeforeEach
	public void setUp() throws Exception {
		menuItem = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
		menuItem2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BaconCheeseBurger.png");
		miList.add(menuItem);
		miList.add(menuItem2);
		when(miDao.findByItemId(1)).thenReturn(menuItem);
		when(miDao.findByItemId(0)).thenReturn(null);
		when(miDao.findAll()).thenReturn(miList);
	}
	
	@Test
	public void getByMenuItemIdSuccess() {
		assertEquals(miServ1.getById(1),menuItem);
	}
	
	@Test
	public void getByMenuItemIdFailure() {
		assertEquals(miServ1.getById(0),null);
	}
	
	@Test
	public void insertNewCouponSuccess() {
		miServ2.insertMenuItem(menuItem);
		verify(miServ2, times(1)).insertMenuItem(menuItem);
	}
	
	@Test
	public void getAllMenuItemSuccess() {
		List<MenuItem> tempList = new ArrayList<MenuItem>();
		tempList = miServ1.getAll();
		System.out.println(tempList);
		boolean check = true;
		if (tempList.isEmpty()) {
			check = false;
		}
		for (int i=0; i<tempList.size(); i++) {
			if (tempList.get(i).equals(miList.get(i))) {
				//do nothing
			} else {
				check = false;
			}
		}
		assertTrue(check);
	}
	
}
