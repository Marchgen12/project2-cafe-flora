package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.dao.OrderDao;
import com.projectcafeflora.dao.OrderItemDao;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderItem;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderItemService;

@SpringBootTest
public class OrderItemServiceUnitTest {

	@Mock
	private OrderItemDao oDao;
	@Mock
	private OrderDao orderDao;
	@Mock
	private MenuItemDao menuItemDao;
	
	@InjectMocks
	private OrderItemService oServ;
	
	@InjectMocks
	private OrderItemService oServ2 = mock(OrderItemService.class);
	
	private Customer customer;
	private OrderStatus orderStatus;
	private Coupon coupon;
	private CouponStatus couponStatus;
	private Distribution distribution;
	private Timestamp timestamp;
	
	private MenuItem menuItem;
	private MenuItem menuItem2;
	private MenuItem menuItem3;
	
	private Order order;
	private OrderItem orderItem;
	private OrderItem orderItem2;
	private OrderItem orderItem3;
	private List<OrderItem> orderItemList = new ArrayList<>();
	private List<OrderItem> emptyList = new ArrayList<>();
	
	
	private MockMvc mock;
	
	@BeforeEach
	public void setUp() throws Exception {
		
		timestamp =  new Timestamp(System.currentTimeMillis());
		distribution = new Distribution(1, "Delivery");
		couponStatus = new CouponStatus(1, "Active");
		coupon = new Coupon("pancakespecial", menuItem, couponStatus, -1.00);
		orderStatus = new OrderStatus(1, "Building");
		customer = new Customer(1, "test@email.com", "pass", "Test", "Customer", "123 street", "Fakesville", "Iowa", "123456", "1231234567", "9876598765987650", "Test Customer", 999, 1, 20125);
		order = new Order(1, customer, orderStatus, distribution, coupon, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);
		
		menuItem = new MenuItem(1,"Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, null);
		menuItem2 = new MenuItem(2,"Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla", 6.50, null);
		menuItem3 = new MenuItem(3,"Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, null);
		
		orderItem = new OrderItem(1, order, menuItem, 1, "extra syrup");
		orderItem2 = new OrderItem(2, order, menuItem2, 1, null);
		orderItem3 = new OrderItem(3, order, menuItem3, 1, null);
		
		orderItemList.add(orderItem);
		orderItemList.add(orderItem2);
		orderItemList.add(orderItem3);
		
		when(oDao.findByOrderItemId(1)).thenReturn(orderItem);
		when(oDao.findByOrderItemId(3)).thenReturn(null);
	
		when(orderDao.findByOrderId(order.getOrderId())).thenReturn(order);
		when(oDao.findByOrder(order)).thenReturn(orderItemList);
		
		when(menuItemDao.findByItemId(menuItem.getItemId())).thenReturn(menuItem);
		when(oDao.findByOrderAndMenuItem(order, menuItem)).thenReturn(orderItem);
	}
	
	@Test
	public void testInsertOrderItemSuccess() {
		oServ2.insertOrderItem(order.getOrderId(), menuItem.getItemId(), orderItem.getQuantity(), orderItem.getSpecialInstructions());
		verify(oServ2, times(1)).insertOrderItem(order.getOrderId(), menuItem.getItemId(), orderItem.getQuantity(), orderItem.getSpecialInstructions());
	}
	
	@Test
	public void testInsertOrderItemFailure() {
		oServ2.insertOrderItem(5, menuItem.getItemId(), orderItem.getQuantity(), orderItem.getSpecialInstructions());
		verify(oServ2, times(0)).insertOrderItem(order.getOrderId(), menuItem.getItemId(), orderItem.getQuantity(), orderItem.getSpecialInstructions());
	}
	
	@Test
	public void testGetByOrderItemIdSuccess() {
		assertEquals(oServ.getById(1), orderItem);
	}
	
	@Test
	public void testGetByOrderItemIdFailure() {
		assertEquals(oServ.getById(3), null);
	}
	
	@Test
	public void testGetByOrderIdSuccess() {
		assertEquals(oServ.getByOrderId(order.getOrderId()), orderItemList);
	}
	
	@Test
	public void testGetByOrderIdFailure() {
		assertEquals(oServ.getByOrderId(5), emptyList);
	}
	
	@Test
	public void testGetByOrderAndMenuIdSuccess() {
		assertEquals(oServ.getByOrderAndMenuItem(order.getOrderId(), menuItem.getItemId()), orderItem);
	}
	
	@Test
	public void testGetByOrderAndMenuIdFailure() {
		assertEquals(oServ.getByOrderAndMenuItem(order.getOrderId(), 3), null);
	}
	
	@Test
	public void testUpdateOrderItemSuccess() {
		oServ2.updateOrderItem(orderItem.getOrderItemId(), orderItem.getQuantity(), null);
		verify(oServ2, times(1)).updateOrderItem(orderItem.getOrderItemId(), orderItem.getQuantity(), null);
	}
	
	@Test
	public void testUpdateOrderItemFailure() {
		oServ2.updateOrderItem(5, orderItem.getQuantity(), null);
		verify(oServ2, times(0)).updateOrderItem(orderItem.getOrderItemId(), orderItem.getQuantity(), null);
	}
	
	@Test
	public void testDeleteByOrderItemIdSuccess() {
		oServ2.deleteById(orderItem.getOrderItemId());
		verify(oServ2, times(1)).deleteById(orderItem.getOrderItemId());
	}
	
	@Test
	public void testDeleteByOrderItemIdFailure() {
		oServ2.deleteById(6);
		verify(oServ2, times(0)).deleteById(orderItem.getOrderItemId());
	}
	
	@Test
	public void testDeleteByOrderIdSuccess() {
		oServ2.deleteByOrderId(order.getOrderId());
		verify(oServ2, times(1)).deleteByOrderId(order.getOrderId());
	}
	
	@Test
	public void testDeleteByOrderIdFailure() {
		oServ2.deleteByOrderId(5);
		verify(oServ2, times(0)).deleteByOrderId(order.getOrderId());
	}
	
	
}
