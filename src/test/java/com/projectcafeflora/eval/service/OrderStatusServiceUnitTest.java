package com.projectcafeflora.eval.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import com.projectcafeflora.dao.OrderStatusDao;
import com.projectcafeflora.model.OrderStatus;
import com.projectcafeflora.service.OrderStatusService;

@SpringBootTest
public class OrderStatusServiceUnitTest {

	@Mock
	OrderStatusDao osDao;
	
	@InjectMocks
	OrderStatusService osServ;
	
	OrderStatus orderStatus;
	
	@BeforeEach
	public void setUp() throws Exception {
		orderStatus = new OrderStatus(1, "Building");
		when(osDao.findByStatusId(1)).thenReturn(orderStatus);
		when(osDao.findByStatusId(0)).thenReturn(null);
	}
	
	@Test
	public void getByIdSuccess() {
		assertEquals(osServ.getById(1), orderStatus);
	}
	
	@Test
	public void getByIdFailure() {
		assertEquals(osServ.getById(0), null);
	}
}
