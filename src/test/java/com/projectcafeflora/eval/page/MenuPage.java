package com.projectcafeflora.eval.page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MenuPage {

	private WebDriver driver;

	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div/div/div/div/div[3]/button[@class='addbutton']")
	//@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div[1]/div/div/div/div[3]/button[@class='addbutton']")
	private WebElement addBurritoButton;
	
//	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div[1]/div/div/div/div[1]/button[@class='addbutton']")
	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div/div/div/div/div[1]/button[@class='addbutton']")
	private WebElement addBaconButton;
	
//	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div[1]/div/div/div/div[10]/button[@class='addbutton']")
	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div/div/div/div/div[10]/button[@class='addbutton']")
	private WebElement addCoffeeButton;
	
//	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div[1]/div/div/div/div[27]/button[@class='addbutton']")
	@FindBy(xpath = "/html/body/app-root/app-customer-menu/body/div/div/div/div/div/div/div[27]/button[@class='addbutton']")
	private WebElement addWaffleButton;
	
	@FindBy(xpath = "//*[@id='submit1']")
	private WebElement goToCartButton;
	
	@FindBy(xpath = "//*[@id='cart']")
	private WebElement navbarCartButton;
	
	@FindBy(xpath = "//*[@id='pagetitle']")
	private WebElement pageHeader;
	
	@FindBy(xpath = "//*[@id='Bacon']")
	private WebElement baconElement;
	
	@FindBy(xpath = "//*[@id='Coffee']")
	private WebElement coffeeElement;
	
	@FindBy(xpath = "//*[@id='Waffles']")
	private WebElement waffleElement;
	
	//Need a button to id pictures
	
	public MenuPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
//		this.addBurritoButton = driver.findElement(By.className("addbutton"));
	}
	
	public void clickAddBurritoButton() {
		this.addBurritoButton.click();
	}
	
	public void selectCoffeeImage() {
		//this.coffeeElement.findElement(by);
		this.coffeeElement.getRect();
		this.coffeeElement.getSize();
		this.coffeeElement.isDisplayed();
	}
	
	public void addMultipleItems() {
		this.addBaconButton.click();
		this.addCoffeeButton.click();
		this.addWaffleButton.click();
	}
	
	public void addMultipleItemsGoToCart() {
		this.addBaconButton.click();
		this.addCoffeeButton.click();
		this.addWaffleButton.click();
		//this.goToCartButton.click();
		clickGoToCartButton();
	}
	
	public void addSameItemMultipleTimes() {
		this.addBurritoButton.click();
		this.addBurritoButton.click();
		this.addBurritoButton.click();
		//this.goToCartButton.click();
		clickGoToCartButton();
	}
	
	public void clickGoToCartButton() {
		this.goToCartButton.click();
	}
	
	public String getPageHeader() {
		return this.pageHeader.getText();
	}
	
	public void clickNavbarCartButton() {
		this.navbarCartButton.click();
	}
	
	public void clickAddBaconButton() {
		this.addBaconButton.click();
	}
}
