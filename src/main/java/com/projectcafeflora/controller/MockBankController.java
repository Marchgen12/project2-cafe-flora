package com.projectcafeflora.controller;

import java.util.LinkedHashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projectcafeflora.model.MockBank;
import com.projectcafeflora.service.MockBankService;
import com.projectcafeflora.service.OrderService;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@CrossOrigin("*")
@RestController
@RequestMapping(value="/mockbanks")
@AllArgsConstructor(onConstructor=@__(@Autowired))
@NoArgsConstructor

public class MockBankController {
	
	private MockBankService bankServ;

	@GetMapping("/creditcardnumbers/{creditcardnumber}")
	public ResponseEntity<MockBank> getCreditCardByNumber(@PathVariable("creditcardnumber") String ccNumber) {
		if (bankServ.getCreditCardByNumber(ccNumber)== null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(bankServ.getCreditCardByNumber(ccNumber), HttpStatus.OK);
	}
	
	@GetMapping("/creditcardname/{creditcardnumber}")
	public ResponseEntity<MockBank> getCreditCardByName(@PathVariable("creditcardnumber") String ccName){
		if(bankServ.getCreditCardByName(ccName) == null) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(bankServ.getCreditCardByName(ccName), HttpStatus.OK);
	}
	
	@PostMapping()
	public ResponseEntity<String> insertCardNumber(@RequestBody LinkedHashMap<String, String> map){
		String creditCardNumber = map.get("creditCardNumber");
		String creditCardName = map.get("creditCardName");
		int creditCardCode = Integer.parseInt(map.get("creditCardCode"));
		int creditCardMonth = Integer.parseInt(map.get("creditCardMonth"));
		int creditCardYear = Integer.parseInt(map.get("creditCardYear"));
		double creditCardAmount = Double.parseDouble(map.get("creditCardAmount"));
		MockBank bank = new MockBank(creditCardNumber, creditCardAmount, creditCardName, creditCardCode, creditCardMonth, creditCardYear); 
		bankServ.insertMockBank(bank);
		return new ResponseEntity<>("MockBank was created", HttpStatus.CREATED);
	}
	
}
