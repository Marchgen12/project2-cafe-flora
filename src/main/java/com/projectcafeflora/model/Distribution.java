package com.projectcafeflora.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="distribution")
public class Distribution {

	@Id
	@Column(name="distribution_id")
	@Setter(AccessLevel.NONE)
	private int distributionId;
	
	@Column(name="distribution_method", nullable=false)
	private String distributionMethod;
	
}
