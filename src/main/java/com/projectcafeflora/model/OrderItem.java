package com.projectcafeflora.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="order_item")
public class OrderItem implements Serializable {
	
	@Id
	@Column(name="order_item_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private int orderItemId;
	
	@ManyToOne(fetch=FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "order_id")
    private Order order;
	
	@ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "item_id")
    private MenuItem menuItem;
	
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="special_instructions")
	private String specialInstructions;

	public OrderItem(Order order, MenuItem menuItem, int quantity, String specialInstructions) {
		super();
		this.order = order;
		this.menuItem = menuItem;
		this.quantity = quantity;
		this.specialInstructions = specialInstructions;
	}

	public OrderItem(int orderItemId, Order order, MenuItem menuItem, int quantity) {
		super();
		this.orderItemId = orderItemId;
		this.order = order;
		this.menuItem = menuItem;
		this.quantity = quantity;
	}

	public OrderItem(Order order, MenuItem menuItem, int quantity) {
		super();
		this.order = order;
		this.menuItem = menuItem;
		this.quantity = quantity;
	}
	
}
