package com.projectcafeflora.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter						
@Setter						
@NoArgsConstructor			
@AllArgsConstructor			
@ToString					 
@Entity
@Table(name="customer")
public class Customer implements Serializable {
	
	@Id
	@Column(name="customer_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Setter(AccessLevel.NONE)
	private int customerId;
	
	@Column(name="email", unique=true, nullable=false)
	private String email;
	
	@Column(name="password", nullable=false)
	private String password;
	
	@Column(name="first_name", nullable=false)
	private String firstName;
	
	@Column(name="last_name", nullable=false)
	private String lastName;
	
	@Column(name="address")
	private String address;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="zip_code")
	private String zipCode;
	
	@Column(name="phone", nullable=false)
	private String phone;
	
	//I have it as long because int can't handle 16numbers
	@Column(name="credit_card_number")
	private String creditCardNumber;
	
	@Column(name="credit_card_name")
	private String creditCardName;
	
	@Column(name="credit_card_code")
	private int creditCardCode;
	
	@Column(name="credit_card_month")
	private int creditCardMonth;
	
	@Column(name="credit_card_year")
	private int creditCardYear;

	public Customer(String email, String password, String firstName, String lastName, String address, String city,
			String state, String zipCode, String phone, String creditCardNumber, String creditCardName,
			int creditCardCode, int creditCardMonth, int creditCardYear) {
		super();
		this.email = email;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
		this.phone = phone;
		this.creditCardNumber = creditCardNumber;
		this.creditCardName = creditCardName;
		this.creditCardCode = creditCardCode;
		this.creditCardMonth = creditCardMonth;
		this.creditCardYear = creditCardYear;
	}

}
