package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.MenuItem;

public interface MenuItemDao extends JpaRepository<MenuItem, Integer> {
	
	public MenuItem findByItemId(int itemId);
	
	public List<MenuItem> findAll();

}
