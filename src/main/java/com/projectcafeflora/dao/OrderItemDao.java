package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderItem;

public interface OrderItemDao extends JpaRepository<OrderItem, Integer> {
	
	public OrderItem findByOrderItemId(int orderItemId);
	
	public List<OrderItem> findByOrder(Order order);
	
	public void deleteByOrder(Order order);
	
	//new
	public OrderItem findByOrderAndMenuItem(Order order, MenuItem menuItem);

}
