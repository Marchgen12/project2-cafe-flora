package com.projectcafeflora.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderStatus;

public interface OrderDao extends JpaRepository<Order, Integer> {
	
	public Order findByOrderId(int orderId);
	
	public List<Order> findAll();
	
	public List<Order> findByOrderStatus(OrderStatus orderStatus);
	
	public List<Order> findByOrderStatusAndCustomer(OrderStatus orderStatus, Customer customer);
	
}
