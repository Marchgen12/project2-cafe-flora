package com.projectcafeflora.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.CouponStatusDao;
import com.projectcafeflora.model.CouponStatus;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CouponStatusService {
	
	private CouponStatusDao csDao;
	
//	Read Methods
	public CouponStatus getById(int id) {
		return csDao.findById(id);
	}
}
