package com.projectcafeflora.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projectcafeflora.dao.CouponDao;
import com.projectcafeflora.dao.CouponStatusDao;
import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.MenuItem;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Service
@NoArgsConstructor
@AllArgsConstructor(onConstructor=@__(@Autowired))
public class CouponService {
	
	private CouponDao coupDao;
	private CouponStatusDao coupStDao;
	private MenuItemDao miDao;
	
	//get by coupon code
	public Coupon getByCouponCode(String couponCode) {
		return coupDao.findBycouponCode(couponCode);
	}
	
//	Insert Methods
	public void insertNewCoupon(String couponCode, int itemId, int couponStatusId, double couponAmount) {
//	public void insertNewCoupon(Coupon coupon) {
		MenuItem menuItem = miDao.findByItemId(itemId);
		CouponStatus couponStatus = coupStDao.findById(couponStatusId);
		Coupon newCoup = new Coupon(couponCode, menuItem, couponStatus, couponAmount); 
		coupDao.save(newCoup);
	}

}
