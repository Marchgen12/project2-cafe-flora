package com.projectcafeflora;

import java.io.File;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.projectcafeflora.dao.CouponDao;
import com.projectcafeflora.dao.CouponStatusDao;
import com.projectcafeflora.dao.CustomerDao;
import com.projectcafeflora.dao.DistributionDao;
import com.projectcafeflora.dao.EmployeeDao;
import com.projectcafeflora.dao.MenuItemDao;
import com.projectcafeflora.dao.MockBankDao;
import com.projectcafeflora.dao.OrderDao;
import com.projectcafeflora.dao.OrderItemDao;
import com.projectcafeflora.dao.OrderStatusDao;
import com.projectcafeflora.model.Coupon;
import com.projectcafeflora.model.CouponStatus;
import com.projectcafeflora.model.Customer;
import com.projectcafeflora.model.Distribution;
import com.projectcafeflora.model.Employee;
import com.projectcafeflora.model.MenuItem;
import com.projectcafeflora.model.MockBank;
import com.projectcafeflora.model.Order;
import com.projectcafeflora.model.OrderItem;
import com.projectcafeflora.model.OrderStatus;

@SpringBootApplication
public class ProjectCafeFloraApplication {

	public final static Logger log = Logger.getLogger(ProjectCafeFloraApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(ProjectCafeFloraApplication.class, args);
		log.setLevel(Level.TRACE);
		
		File file = new File("src/main/resources/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
	
	}

//	@Bean
	public CommandLineRunner loadOrderStatus(OrderStatusDao osDao) {
		return (args) -> {
			OrderStatus os1 = new OrderStatus(1, "Building");
			OrderStatus os2 = new OrderStatus(2, "Ordered");
			OrderStatus os3 = new OrderStatus(3, "Preparing");
			OrderStatus os4 = new OrderStatus(4, "Ready");
			OrderStatus os5 = new OrderStatus(5, "Delivering");
			OrderStatus os6 = new OrderStatus(6, "Completed");
			OrderStatus os7 = new OrderStatus(7, "Canceled");
			
			osDao.save(os1);
			osDao.save(os2);
			osDao.save(os3);
			osDao.save(os4);
			osDao.save(os5);
			osDao.save(os6);
			osDao.save(os7);
		
		};
	}
	
//	@Bean
	public CommandLineRunner loadMenuItem(MenuItemDao miDao) {
		return (args) -> {
			
			MenuItem mi1 = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
			MenuItem mi2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BaconCheeseBurger.png");
			MenuItem mi3 = new MenuItem(3,"Breakfast Burrito", "Sausage, egg, and cheese in a flour tortilla", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BreakfastBurrito.png");
			MenuItem mi4 = new MenuItem(4,"Caesar Salad", "Romaine lettuce, parmesean cheese, and croutons with caesar dressing", 5.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/caesar_salad.png");
			MenuItem mi5 = new MenuItem(5,"Chef Salad", "Large salad including ham, turkey, cheese, hard boiled egg, tomato, and cucumbers", 6.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChefSalad.png");
			MenuItem mi6 = new MenuItem(6,"Chicken Noodle Soup", "Hot nourishing chicken noodle soup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenNoodleSoup.png");
			MenuItem mi7 = new MenuItem(7,"Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenTenders.png");
			MenuItem mi8 = new MenuItem(8,"Chili Cheese Fries", "Fries smothered in chili and cheese", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChiliCheeseFries.png");
			MenuItem mi9 = new MenuItem(9,"Club Sandwich", "Turkey, bacon, lettuce, and tomato layered between two slices of toast", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ClubSandwich.png");
			MenuItem mi10 = new MenuItem(10,"Coffee", "A great cup of joe", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/coffee.png");
			MenuItem mi11 = new MenuItem(11,"Coney Dog", "Grilled Hotdog covered in chili and cheese, fries included", 4.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ConeyDog.png");
			MenuItem mi12 = new MenuItem(12,"Donut", "Assortment of donuts, made fresh every morning", 1.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/dount.png");
			MenuItem mi13 = new MenuItem(13,"Eggs", "Three eggs cooked to order", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Eggs.png");
			MenuItem mi14 = new MenuItem(14,"Lasagna", "Featuring Italian sausage, ground beef, ricotta cheese, mozzarella cheese, and parmesan cheese", 8.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Lasagna.png");
			MenuItem mi15 = new MenuItem(15,"Meatloaf", "Tender and juicy meatloaf with a sweet and tangy glaze", 8.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/MeatLoaf.png");
			MenuItem mi16 = new MenuItem(16,"Muffin", "Available in bran, blueberry, and chococlate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/muffin.png");
			MenuItem mi17 = new MenuItem(17,"Nachos", "Classic nachoes with ground beef, american cheese, tomatos, olives, and jalapenos", 7.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Nachos.png");
			MenuItem mi18 = new MenuItem(18,"Omlette", "Three egg omlette filled with cheese, sausage, ham, onions, and green peppers", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Omlette.png");
			MenuItem mi19 = new MenuItem(19,"Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Pancakes.png");
			MenuItem mi20 = new MenuItem(20,"Sausage", "Plate of fried sausage links", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Sausage.png");
			MenuItem mi21 = new MenuItem(21,"Sausage Egg Muffin", "Sausage, egg, and cheese on a muffin, what more do you want?", 2.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/SausageEggMuffin.png");
			MenuItem mi22 = new MenuItem(22,"Scone", "Available in lemon, blueberry, and chocolate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/scone.png");
			MenuItem mi23 = new MenuItem(23,"Spaghetti", "Spaghetti noodles covered in meat sause", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Spaghetti.png");
			MenuItem mi24 = new MenuItem(24,"Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/spinach_wrap.png");
			MenuItem mi25 = new MenuItem(25,"Tea", "Black, green, or sweet", 1.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/tea.png");
			MenuItem mi26 = new MenuItem(26,"Tomato Soup", "Bowl of hot tomato soup, comes with a grilled cheese sandwich", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/TomatoSoupWithToastedCheeseSandwich.png");
			MenuItem mi27 = new MenuItem(27,"Waffles", "Stack of crisp waffles covered in butter and syrup", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Waffles.png");
			
			miDao.save(mi1);
			miDao.save(mi2);
			miDao.save(mi3);
			miDao.save(mi4);
			miDao.save(mi5);
			miDao.save(mi6);
			miDao.save(mi7);
			miDao.save(mi8);
			miDao.save(mi9);
			miDao.save(mi10);
			miDao.save(mi11);
			miDao.save(mi12);
			miDao.save(mi13);
			miDao.save(mi14);
			miDao.save(mi15);
			miDao.save(mi16);
			miDao.save(mi17);
			miDao.save(mi18);
			miDao.save(mi19);
			miDao.save(mi20);
			miDao.save(mi21);
			miDao.save(mi22);
			miDao.save(mi23);
			miDao.save(mi24);
			miDao.save(mi25);
			miDao.save(mi26);
			miDao.save(mi27);
		};
	}
	
//	@Bean
	public CommandLineRunner loadDistribution(DistributionDao dnDao) {
		return (args) -> {
			Distribution dn1 = new Distribution(1, "Delivery");
			Distribution dn2 = new Distribution(2, "Pickup");
			
			dnDao.save(dn1);
			dnDao.save(dn2);
		};
	}
	
//	@Bean
	public CommandLineRunner loadCouponStatus(CouponStatusDao csDao) {
		return (args) -> {
			CouponStatus cs1 = new CouponStatus(1, "Active");
			CouponStatus cs2 = new CouponStatus(2, "Inactive");
			
			csDao.save(cs1);
			csDao.save(cs2);
		};
	}
	
//	@Bean
	public CommandLineRunner loadCoupon(CouponDao cnDao) {
		return (args) -> {
			CouponStatus cs1 = new CouponStatus(1, "Active");
			CouponStatus cs2 = new CouponStatus(2, "Inactive");
			
			MenuItem mi1 = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
			MenuItem mi2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BaconCheeseBurger.png");
			MenuItem mi3 = new MenuItem(3,"Breakfast Burrito", "Sausage, egg, and cheese in a flour tortilla", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BreakfastBurrito.png");
			MenuItem mi4 = new MenuItem(4,"Caesar Salad", "Romaine lettuce, parmesean cheese, and croutons with caesar dressing", 5.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/caesar_salad.png");
			MenuItem mi5 = new MenuItem(5,"Chef Salad", "Large salad including ham, turkey, cheese, hard boiled egg, tomato, and cucumbers", 6.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChefSalad.png");
			MenuItem mi6 = new MenuItem(6,"Chicken Noodle Soup", "Hot nourishing chicken noodle soup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenNoodleSoup.png");
			MenuItem mi7 = new MenuItem(7,"Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenTenders.png");
			MenuItem mi8 = new MenuItem(8,"Chili Cheese Fries", "Fries smothered in chili and cheese", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChiliCheeseFries.png");
			MenuItem mi9 = new MenuItem(9,"Club Sandwich", "Turkey, bacon, lettuce, and tomato layered between two slices of toast", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ClubSandwich.png");
			MenuItem mi10 = new MenuItem(10,"Coffee", "A great cup of joe", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/coffee.png");
			MenuItem mi11 = new MenuItem(11,"Coney Dog", "Grilled Hotdog covered in chili and cheese, fries included", 4.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ConeyDog.png");
			MenuItem mi12 = new MenuItem(12,"Donut", "Assortment of donuts, made fresh every morning", 1.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/dount.png");
			MenuItem mi13 = new MenuItem(13,"Eggs", "Three eggs cooked to order", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Eggs.png");
			MenuItem mi14 = new MenuItem(14,"Lasagna", "Featuring Italian sausage, ground beef, ricotta cheese, mozzarella cheese, and parmesan cheese", 8.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Lasagna.png");
			MenuItem mi15 = new MenuItem(15,"Meatloaf", "Tender and juicy meatloaf with a sweet and tangy glaze", 8.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/MeatLoaf.png");
			MenuItem mi16 = new MenuItem(16,"Muffin", "Available in bran, blueberry, and chococlate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/muffin.png");
			MenuItem mi17 = new MenuItem(17,"Nachos", "Classic nachoes with ground beef, american cheese, tomatos, olives, and jalapenos", 7.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Nachos.png");
			MenuItem mi18 = new MenuItem(18,"Omlette", "Three egg omlette filled with cheese, sausage, ham, onions, and green peppers", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Omlette.png");
			MenuItem mi19 = new MenuItem(19,"Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Pancakes.png");
			MenuItem mi20 = new MenuItem(20,"Sausage", "Plate of fried sausage links", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Sausage.png");
			MenuItem mi21 = new MenuItem(21,"Sausage Egg Muffin", "Sausage, egg, and cheese on a muffin, what more do you want?", 2.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/SausageEggMuffin.png");
			MenuItem mi22 = new MenuItem(22,"Scone", "Available in lemon, blueberry, and chocolate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/scone.png");
			MenuItem mi23 = new MenuItem(23,"Spaghetti", "Spaghetti noodles covered in meat sause", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Spaghetti.png");
			MenuItem mi24 = new MenuItem(24,"Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/spinach_wrap.png");
			MenuItem mi25 = new MenuItem(25,"Tea", "Black, green, or sweet", 1.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/tea.png");
			MenuItem mi26 = new MenuItem(26,"Tomato Soup", "Bowl of hot tomato soup, comes with a grilled cheese sandwich", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/TomatoSoupWithToastedCheeseSandwich.png");
			MenuItem mi27 = new MenuItem(27,"Waffles", "Stack of crisp waffles covered in butter and syrup", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Waffles.png");
			
			Coupon cn1 = new Coupon("halfoffchickensoup", mi5, cs1, -1.50);
			Coupon cn2 = new Coupon("halfoffburrito", mi3, cs1, -1.75);
			Coupon cn3 = new Coupon("meatloafspecial", mi15, cs1, -2.00);
			Coupon cn4 = new Coupon("pancakespecial", mi19, cs1, -1.00);
			Coupon cn5 = new Coupon("baconcheeseburger", mi2, cs1, -3.00);
			Coupon cn6 = new Coupon("chilicheesefries", mi8, cs1, -1.00);
			
			cnDao.save(cn1);
			cnDao.save(cn2);
			cnDao.save(cn3);
			cnDao.save(cn4);
			cnDao.save(cn5);
			cnDao.save(cn6);
		};
	}
	
//	@Bean
	public CommandLineRunner loadCustomer(CustomerDao crDao) {
		return (args) -> {

			//I rearranged email <=> password
			String encrypted = "S15nC0XLiN9N9fJJR/Im05HUlRZcwbfw";
			String encryptedCC1 = "MdQyoeUwysSodpzU7kgEMyDqUV5cFAIw159iJwafVZQ=";
			Customer cr1 = new Customer("email@email.com", encrypted, "Daniel", "Butcher", "123 Main St", "New City", "NY", "01012", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
			Customer cr2 = new Customer("johnsmith@email.com", encrypted, "John", "Smith", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "John Smith", 999, 1, 2025);
			Customer cr3 = new Customer("dbutcher42@comcast.net", encrypted, "Daniel", "Butcher", "123 Main St", "New City", "NY", "01234", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);

			crDao.save(cr1);
			crDao.save(cr2);
			crDao.save(cr3);
			
		};
	}
	
//	@Bean
	public CommandLineRunner loadOrder(OrderDao orDao) {
		return (args) -> {

			Distribution dn1 = new Distribution(1, "Delivery");
			Distribution dn2 = new Distribution(2, "Pickup");
			
			OrderStatus os1 = new OrderStatus(1, "Building");
			OrderStatus os2 = new OrderStatus(2, "Ordered");
			OrderStatus os3 = new OrderStatus(3, "Preparing");
			OrderStatus os4 = new OrderStatus(4, "Ready");
			OrderStatus os5 = new OrderStatus(5, "Delivering");
			OrderStatus os6 = new OrderStatus(6, "Completed");
			OrderStatus os7 = new OrderStatus(7, "Canceled");

			MenuItem mi1 = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
			MenuItem mi2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BaconCheeseBurger.png");
			MenuItem mi3 = new MenuItem(3,"Breakfast Burrito", "Sausage, egg, and cheese in a flour tortilla", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BreakfastBurrito.png");
			MenuItem mi4 = new MenuItem(4,"Caesar Salad", "Romaine lettuce, parmesean cheese, and croutons with caesar dressing", 5.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/caesar_salad.png");
			MenuItem mi5 = new MenuItem(5,"Chef Salad", "Large salad including ham, turkey, cheese, hard boiled egg, tomato, and cucumbers", 6.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChefSalad.png");
			MenuItem mi6 = new MenuItem(6,"Chicken Noodle Soup", "Hot nourishing chicken noodle soup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenNoodleSoup.png");
			MenuItem mi7 = new MenuItem(7,"Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenTenders.png");
			MenuItem mi8 = new MenuItem(8,"Chili Cheese Fries", "Fries smothered in chili and cheese", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChiliCheeseFries.png");
			MenuItem mi9 = new MenuItem(9,"Club Sandwich", "Turkey, bacon, lettuce, and tomato layered between two slices of toast", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ClubSandwich.png");
			MenuItem mi10 = new MenuItem(10,"Coffee", "A great cup of joe", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/coffee.png");
			MenuItem mi11 = new MenuItem(11,"Coney Dog", "Grilled Hotdog covered in chili and cheese, fries included", 4.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ConeyDog.png");
			MenuItem mi12 = new MenuItem(12,"Donut", "Assortment of donuts, made fresh every morning", 1.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/dount.png");
			MenuItem mi13 = new MenuItem(13,"Eggs", "Three eggs cooked to order", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Eggs.png");
			MenuItem mi14 = new MenuItem(14,"Lasagna", "Featuring Italian sausage, ground beef, ricotta cheese, mozzarella cheese, and parmesan cheese", 8.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Lasagna.png");
			MenuItem mi15 = new MenuItem(15,"Meatloaf", "Tender and juicy meatloaf with a sweet and tangy glaze", 8.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/MeatLoaf.png");
			MenuItem mi16 = new MenuItem(16,"Muffin", "Available in bran, blueberry, and chococlate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/muffin.png");
			MenuItem mi17 = new MenuItem(17,"Nachos", "Classic nachoes with ground beef, american cheese, tomatos, olives, and jalapenos", 7.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Nachos.png");
			MenuItem mi18 = new MenuItem(18,"Omlette", "Three egg omlette filled with cheese, sausage, ham, onions, and green peppers", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Omlette.png");
			MenuItem mi19 = new MenuItem(19,"Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Pancakes.png");
			MenuItem mi20 = new MenuItem(20,"Sausage", "Plate of fried sausage links", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Sausage.png");
			MenuItem mi21 = new MenuItem(21,"Sausage Egg Muffin", "Sausage, egg, and cheese on a muffin, what more do you want?", 2.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/SausageEggMuffin.png");
			MenuItem mi22 = new MenuItem(22,"Scone", "Available in lemon, blueberry, and chocolate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/scone.png");
			MenuItem mi23 = new MenuItem(23,"Spaghetti", "Spaghetti noodles covered in meat sause", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Spaghetti.png");
			MenuItem mi24 = new MenuItem(24,"Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/spinach_wrap.png");
			MenuItem mi25 = new MenuItem(25,"Tea", "Black, green, or sweet", 1.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/tea.png");
			MenuItem mi26 = new MenuItem(26,"Tomato Soup", "Bowl of hot tomato soup, comes with a grilled cheese sandwich", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/TomatoSoupWithToastedCheeseSandwich.png");
			MenuItem mi27 = new MenuItem(27,"Waffles", "Stack of crisp waffles covered in butter and syrup", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Waffles.png");

			CouponStatus cs1 = new CouponStatus(1, "Active");
			CouponStatus cs2 = new CouponStatus(2, "Inactive");
			
			Coupon cn1 = new Coupon("halfoffchickensoup", mi5, cs1, -1.50);
			Coupon cn2 = new Coupon("halfoffburrito", mi3, cs1, -1.75);
			Coupon cn3 = new Coupon("meatloafspecial", mi15, cs1, -2.00);
			Coupon cn4 = new Coupon("pancakespecial", mi19, cs1, -1.00);
			Coupon cn5 = new Coupon("baconcheeseburger", mi2, cs1, -3.00);
			Coupon cn6 = new Coupon("chilicheesefries", mi8, cs1, -1.00);
			
			String encrypted = "S15nC0XLiN9N9fJJR/Im05HUlRZcwbfw";
			String encryptedCC1 = "MdQyoeUwysSodpzU7kgEMyDqUV5cFAIw159iJwafVZQ=";
			Customer cr1 = new Customer(1, "email@email.com", encrypted, "Daniel", "Butcher", "123 Main St", "New City", "NY", "01012", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
			Customer cr2 = new Customer(2, "johnsmith@email.com", encrypted, "John", "Smith", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "John Smith", 999, 1, 2025);
			
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			
			Order or1 = new Order(3, cr1, os2, dn1, cn1, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);
			Order or2 = new Order(4, cr2, os5, dn2, cn2, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);
			Order or3 = new Order(5, cr1, os6, dn1, null, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);

			orDao.save(or1);
			orDao.save(or2);
			orDao.save(or3);
			
//			orDao.delete(or3);

			
		};
	}
	
//	@Bean
	public CommandLineRunner loadOrderItem(OrderItemDao oiDao) {
		return (args) -> {
			
			Distribution dn1 = new Distribution(1, "Delivery");
			Distribution dn2 = new Distribution(2, "Pickup");
			
			CouponStatus cs1 = new CouponStatus(1, "Active");
			CouponStatus cs2 = new CouponStatus(2, "Inactive");
			
			OrderStatus os1 = new OrderStatus(1, "Building");
			OrderStatus os2 = new OrderStatus(2, "Ordered");
			OrderStatus os3 = new OrderStatus(3, "Preparing");
			OrderStatus os4 = new OrderStatus(4, "Ready");
			OrderStatus os5 = new OrderStatus(5, "Delivering");
			OrderStatus os6 = new OrderStatus(6, "Completed");
			OrderStatus os7 = new OrderStatus(7, "Canceled");

			MenuItem mi1 = new MenuItem(1,"Bacon", "Plate of nice crispy bacon", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Bacon.png");
			MenuItem mi2 = new MenuItem(2,"Bacon Cheese Burger", "Juicy bacon cheese burger with all the fixin's, fries included", 9.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BaconCheeseBurger.png");
			MenuItem mi3 = new MenuItem(3,"Breakfast Burrito", "Sausage, egg, and cheese in a flour tortilla", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/BreakfastBurrito.png");
			MenuItem mi4 = new MenuItem(4,"Chef Salad", "Large salad including ham, turkey, cheese, hard boiled egg, tomato, and cucumbers", 6.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChefSalad.png");
			MenuItem mi5 = new MenuItem(5,"Chicken Noodle Soup", "Hot nourishing chicken noodle soup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenNoodleSoup.png");
			MenuItem mi6 = new MenuItem(6,"Chicken Tenders", "Mouthwatering chicken tenders, fries included", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChickenTenders.png");
			MenuItem mi7 = new MenuItem(7,"Chili Cheese Fries", "Fries smothered in chili and cheese", 3.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ChiliCheeseFries.png");
			MenuItem mi8 = new MenuItem(8,"Club Sandwich", "Turkey, bacon, lettuce, and tomato layered between two slices of toast", 8.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ClubSandwich.png");
			MenuItem mi9 = new MenuItem(9,"Coney Dog", "Grilled Hotdog covered in chili and cheese, fries included", 4.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/ConeyDog.png");
			MenuItem mi10 = new MenuItem(10,"Eggs", "Three eggs cooked to order", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Eggs.png");
			MenuItem mi11 = new MenuItem(11,"Lasagna", "Featuring Italian sausage, ground beef, ricotta cheese, mozzarella cheese, and parmesan cheese", 8.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Lasagna.png");
			MenuItem mi12 = new MenuItem(12,"Meatloaf", "Tender and juicy meatloaf with a sweet and tangy glaze", 8.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/MeatLoaf.png");
			MenuItem mi13 = new MenuItem(13,"Nachos", "Classic nachoes with ground beef, american cheese, tomatos, olives, and jalapenos", 7.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Nachos.png");
			MenuItem mi14 = new MenuItem(14,"Omlette", "Three egg omlette filled with cheese, sausage, ham, onions, and green peppers", 4.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Omlette.png");
			MenuItem mi15 = new MenuItem(15,"Pancakes", "Stack of fluffy pancakes covered in butter and syrup", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Pancakes.png");
			MenuItem mi16 = new MenuItem(16,"Sausage", "Plate of fried sausage links", 3.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Sausage.png");
			MenuItem mi17 = new MenuItem(17,"Sausage Egg Muffin", "Sausage, egg, and cheese on a muffin, what more do you want?", 2.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/SausageEggMuffin.png");
			MenuItem mi18 = new MenuItem(18,"Spaghetti", "Spaghetti noodles covered in meat sause", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Spaghetti.png");
			MenuItem mi19 = new MenuItem(19,"Tomato Soup", "Bowl of hot tomato soup, comes with a grilled cheese sandwich", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/TomatoSoupWithToastedCheeseSandwich.png");
			MenuItem mi20 = new MenuItem(20,"Waffles", "Stack of crisp waffles covered in butter and syrup", 4.0, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/Waffles.png");
			MenuItem mi21 = new MenuItem(21,"Donut", "Assortment of donuts, made fresh every morning", 1.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/dount.png");
			MenuItem mi22 = new MenuItem(22,"Muffin", "Available in bran, blueberry, and chococlate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/muffin.png");
			MenuItem mi23 = new MenuItem(23,"Scone", "Available in lemon, blueberry, and chocolate chip", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/scone.png");
			MenuItem mi24 = new MenuItem(24,"Caesar Salad", "Romaine lettuce, parmesean cheese, and croutons with caesar dressing", 5.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/caesar_salad.png");
			MenuItem mi25 = new MenuItem(25,"Spinach Wrap", "Turkey, bacon, avocado, lettuce, and tomato in a spinach tortilla", 6.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/spinach_wrap.png");
			MenuItem mi26 = new MenuItem(26,"Coffee", "A great cup of joe", 2.00, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/coffee.png");
			MenuItem mi27 = new MenuItem(27,"Tea", "Black, green, or sweet", 1.50, "https://projectcafeflora.s3.us-east-2.amazonaws.com/images/tea.png");

			Coupon cn1 = new Coupon("halfoffchickensoup", mi5, cs1, -1.50);
			Coupon cn2 = new Coupon("halfoffburrito", mi3, cs1, -1.75);
			Coupon cn3 = new Coupon("meatloafspecial", mi15, cs1, -2.00);
			Coupon cn4 = new Coupon("pancakespecial", mi19, cs1, -1.00);
			Coupon cn5 = new Coupon("baconcheeseburger", mi2, cs1, -3.00);
			Coupon cn6 = new Coupon("chilicheesefries", mi8, cs1, -1.00);
			
			String encrypted = "S15nC0XLiN9N9fJJR/Im05HUlRZcwbfw";
			String encryptedCC1 = "MdQyoeUwysSodpzU7kgEMyDqUV5cFAIw159iJwafVZQ=";
			Customer cr1 = new Customer(1, "email@email.com", encrypted, "Daniel", "Butcher", "123 Main St", "New City", "NY", "01012", "123 456 7890", encryptedCC1, "Daniel Butcher", 999, 1, 2025);
			Customer cr2 = new Customer(2, "johnsmith@email.com", encrypted, "John", "Smith", "address", "city", "state", "zip", "123 456 7890", encryptedCC1, "John Smith", 999, 1, 2025);
			
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		
			Order or1 = new Order(3, cr1, os2, dn1, cn1, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);
			Order or2 = new Order(4, cr2, os5, dn2, cn2, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);
			Order or3 = new Order(5, cr1, os6, dn1, null, 10.0, 1.0, 2.0, 13.0, timestamp, timestamp, timestamp, timestamp);

			OrderItem oi1 = new OrderItem(or1, mi6, 1, "extra syrup");
			OrderItem oi2 = new OrderItem(or1, mi7, 2, "scrambled");
			OrderItem oi3 = new OrderItem(or1, mi3, 1, null);
			OrderItem oi4 = new OrderItem(or2, mi4, 3, "crispy");
			OrderItem oi5 = new OrderItem(or2, mi5, 2, null);
			
//			oiDao.save(oi1);
//			oiDao.save(oi2);
//			oiDao.save(oi3);
//			oiDao.save(oi4);
//			oiDao.save(oi5);
			
//			oiDao.delete(oi5);
//			oiDao.deleteByOrder(or1);
//			List<OrderItem> miList = oiDao.findByOrder(or1);
//			for (OrderItem oi : miList) {
//				System.out.println(oi);
//			}

		};
	}
	
//	@Bean
	public CommandLineRunner loadEmployee(EmployeeDao emDao) {
		return (args) -> {

			Employee em1 = new Employee("password", "dbutcher42@comcast.net", "Daniel", "Butcher");
			
			emDao.save(em1);
			
		};
	}
	
//	@Bean
	public CommandLineRunner loadMockBank(MockBankDao mbDao) {
		return (args) -> {

			MockBank mb1 = new MockBank("1111111111111111", 5000.0, "Daniel Butcher", 999, 1, 2025);
			MockBank mb2 = new MockBank("2222222222222222", 5.0, "Daniel Butcher", 999, 1, 2025);
			MockBank mb3 = new MockBank("3333333333333333", 250.0, "John Smith", 999, 1, 2025);
			MockBank mb4 = new MockBank("4444444444444444", 250.0, "John Smith", 999, 1, 2025);
			MockBank mb5 = new MockBank("5555555555555555", 250.0, "John Smith", 999, 1, 2025);
			MockBank mb6 = new MockBank("6666666666666666", 250.0, "John Smith", 999, 1, 2025);
			
			mbDao.save(mb1);
			mbDao.save(mb2);
			mbDao.save(mb3);
			mbDao.save(mb4);
			mbDao.save(mb5);
			mbDao.save(mb6);
			
		};
	}
		
}
