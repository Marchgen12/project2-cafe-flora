Feature: Cafe Flora Password Recovery
	As a user I wish to recover my password
	
	Scenario Outline: Recovering password successfully
		Given a user is at the login page of Cafe Flora first time
		When a user clicks the Forget Password link
		And a user inputs their email into the recover password field "<email>"
		And then a user inputs their first name "<first>"
		And then a user inputs their last name "<last>"
		And then the user clicks the send link button
		Then the user is redirected back to the login page
	
	Examples:
		| email                  | first  | last    |
		| dbutcher42@comcast.net | Daniel | Butcher |
		
	Scenario Outline: Recovering password unsuccessfully
		Given a user is at the login page of Cafe Flora first time
		When a user clicks the Forget Password link
		And a user inputs their email into the recover password field "<email>"
		And a user inputs their first name "<first>"
		And a user inputs their last name "<last>"
		And then the user clicks the send link button
		Then the user gets a bad email message
	
	Examples:
		| email                  | first  | last    |
		| dbutcher99@comcast.net | Daniel | Butcher |
		
	Scenario Outline: Leaving the recover password page
		Given a user is at the login page of Cafe Flora first time
		When a user clicks the Forget Password link
		And then the user clicks the back to login button
		Then the user is redirected back to the login page
	
	Examples:
		| |
		| |