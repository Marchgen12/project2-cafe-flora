Feature: Cafe Flora Verification
	As a user I want to see verification my order was entered

	Scenario Outline: Paying for order with saved information and seeing verification
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And a user inputs their coupon code "<couponcode>"
		And a user enters a tip "<tip>"
		And then a user submits to payment
		And then a user clicks on use saved CC info
		And then a user clicks process payment
		And then a user sees the verification page
		And then a user clicks the close verification page button
		Then a user is redirected from the verification page to the home page 
	
	Examples:
		| email           | password | couponcode     | tip |
		| email@email.com | password | halfoffburrito |	2   |