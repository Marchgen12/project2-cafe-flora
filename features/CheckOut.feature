Feature: Cafe Flora Checkout
	As a user I wish to use the checkout features of the checkout page
	
	Scenario Outline: Checking out an order for two burritos
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And a user inputs their coupon code "<couponcode>"
		And a user enters a tip "<tip>"
		And a user clicks pickup
		And a user clicks delivery
		And a user enters a new tip "<tip>"
		And then a user submits to payment
		Then the user is redirected to the payment screen
	
	Examples:
		| email           | password | couponcode     | tip |
		| email@email.com | password | halfoffburrito |	2   |
		
	Scenario Outline: Checking out an order for two burritos with the wrong coupon
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And a user inputs their wrong coupon code "<couponcode2>"
		And then a user submits to payment
		Then the user is redirected to the payment screen
	
	Examples:
		| email           | password | couponcode     | tip | couponcode2     |
		| email@email.com | password | halfoffburrito |	2   | meatloafspecial |
		
	Scenario Outline: Checking out an order for two burritos then going back to cart
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks checkout
		And then a user clicks go back to cart
		Then the user is redirected back to the cart screen
	
	Examples:
		| email           | password |
		| email@email.com | password |
		
		
