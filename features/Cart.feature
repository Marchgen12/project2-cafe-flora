Feature: Cafe Flora Cart
	As a user I wish to use the cart features of the cart page
	
	Scenario Outline: Clearing out all cart items
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add bacon button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks the clear cart button
		Then the cart total should be 0
		
	Examples:
		| email           | password |
		| email@email.com | password |
		
	Scenario Outline: Removing individual cart item
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add bacon button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks the remove button
		Then the cart total should decrease
		
	Examples:
	| email           | password |
	| email@email.com | password |
		
	Scenario Outline: Increasing items quantity
		Given a user is at the login page of Cafe Flora first time
		When a user inputs their email "<email>"
		And a user inputs their password "<password>"
		And then the user submits the information
		And a user clicks the menu button
		And a user clicks the add bacon button
		And a user clicks the add burrito button twice
		And then a user clicks go to cart
		And then a user clicks the add button
		Then the cart total should increase
	
	Examples:
		| email           | password |
		| email@email.com | password |